import React from "react";
import PropTypes from "prop-types";

import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import Favorite from "@material-ui/icons/Favorite";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import { withStyles } from "@material-ui/core/styles";

const IMAGE_URL = "https://image.tmdb.org/t/p/original/";

const styles = () => ({
    gridListTitle: {
        height: 372.5,
        width: 251.21,
        cursor: "pointer"
    },
    titleBar: {
        background: "rgba(0, 0, 0, 0.8)"
    },
    titleBarIcon: {
        marginRight: 19
    }
});

const MoviesGridListItem = ({
    classes,
    isFavorite,
    movie,
    onSelect,
    showFavoriteIcon = false
}) => (
    <GridListTile
        key={movie.id}
        className={classes.gridListTitle}
        onClick={() => onSelect(movie.id)}
    >
        {movie.poster_path ? (
            <img
                src={`${IMAGE_URL}${movie.poster_path}`}
                alt={movie.title}
            />
        ) : (
            <img alt={movie.title} />
        )}
        <GridListTileBar
            title={movie.title}
            subtitle={movie.release_date}
            actionIcon={
                showFavoriteIcon &&
                (isFavorite ? (
                    <Favorite
                        color="primary"
                        className={classes.titleBarIcon}
                    />
                ) : (
                    <FavoriteBorder
                        color="primary"
                        className={classes.titleBarIcon}
                    />
                ))
            }
            className={classes.titleBar}
        />
    </GridListTile>
);

MoviesGridListItem.propTypes = {
    classes: PropTypes.object.isRequired,
    isFavorite: PropTypes.bool.isRequired,
    movie: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
    showFavoriteIcon: PropTypes.bool.isRequired
};

MoviesGridListItem.defaultProps = {
    isFavorite: false,
    onSelect: () => {},
    showFavoriteIcon: false
};

export default withStyles(styles)(MoviesGridListItem);
