import React, { Component } from "react";
import PropTypes from "prop-types";

import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import NavigateBefore from "@material-ui/icons/NavigateBefore";
import NavigateNext from "@material-ui/icons/NavigateNext";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import { withStyles } from "@material-ui/core/styles";

const styles = {
    pages: {
        display: "inline-block"
    }
};

class Pagination extends Component {
    state = {
        page: this.props.data.page,
        results: this.props.data.results,
        totalPages: this.props.data.total_pages,
        totalResults: this.props.data.total_results
    };

    static propTypes = {
        classes: PropTypes.object.isRequired,
        data: PropTypes.shape({
            page: PropTypes.number.isRequired,
            results: PropTypes.array.isRequired,
            total_pages: PropTypes.number.isRequired,
            total_results: PropTypes.number.isRequired
        }).isRequired
    };

    handleBefore(page) {
        if (page !== 1) {
            this.props.fetch("star wars", page - 1).then(
                this.setState({
                    page: page - 1
                })
            );
        }
    }

    handleNext(page, totalPages) {
        if (page !== totalPages) {
            this.props.fetch("star wars", page + 1).then(
                this.setState({
                    page: page + 1
                })
            );
        }
    }

    handleGoTo(page) {
        this.props.fetch("star wars", page).then(
            this.setState({
                page: page
            })
        );
    }

    render() {
        const { classes } = this.props;
        const { page, totalPages } = this.state;
        return (
            <div>
                <IconButton
                    aria-label="first"
                    disabled={page === 1}
                    onClick={() => this.handleGoTo(1)}
                >
                    <FirstPage />
                </IconButton>
                <IconButton
                    aria-label="before"
                    disabled={page === 1}
                    onClick={() => this.handleBefore(page)}
                >
                    <NavigateBefore />
                </IconButton>
                <Typography
                    variant="caption"
                    className={classes.pages}
                >{`${page} - ${totalPages}`}</Typography>
                <IconButton
                    aria-label="next"
                    onClick={() => this.handleNext(page, totalPages)}
                    disabled={page === totalPages}
                >
                    <NavigateNext />
                </IconButton>
                <IconButton
                    aria-label="last"
                    onClick={() => this.handleGoTo(totalPages)}
                    disabled={page === totalPages}
                >
                    <LastPage />
                </IconButton>
            </div>
        );
    }
}

export default withStyles(styles)(Pagination);
