import { RSAA } from "redux-api-middleware";
import ActionTypes from "../constants/ActionTypes";

const API_KEY = "4cb1eeab94f45affe2536f2c684a5c9e";
const endpoint = "https://api.themoviedb.org/3";

export const fetchMovies = (input, page) => dispatch => {
    return dispatch({
        [RSAA]: {
            endpoint: `${endpoint}/search/movie?query=${input}&api_key=${API_KEY}${
                page ? `&page=${page}` : ""
            }`,
            method: "GET",
            types: [
                ActionTypes.FETCH_MOVIES_REQUEST,
                {
                    type: ActionTypes.FETCH_MOVIES_SUCCESS,
                    payload: (action, state, res) => res.json()
                },
                ActionTypes.FETCH_MOVIES_FAILURE
            ]
        }
    });
};

export const fetchMovie = id => dispatch => {
    return dispatch({
        [RSAA]: {
            endpoint: `${endpoint}/movie/${id}?api_key=${API_KEY}`,
            method: "GET",
            types: [
                ActionTypes.FETCH_MOVIE_REQUEST,
                {
                    type: ActionTypes.FETCH_MOVIE_SUCCESS,
                    payload: (action, state, res) => res.json()
                },
                ActionTypes.FETCH_MOVIE_FAILURE
            ]
        }
    });
};

export const addMovieToFavorits = movie => ({
    type: ActionTypes.ADD_MOVIE_TO_FAVORITES,
    payload: movie
});

export const removeMovieFromFavorits = movie => ({
    type: ActionTypes.REMOVE_MOVIE_FROM_FAVORITES,
    payload: movie
});
