import { compose } from "ramda";
import { createStore, applyMiddleware } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import thunk from "redux-thunk";
import storage from "redux-persist/lib/storage";
import { apiMiddleware } from "redux-api-middleware";

import appReducer from "../reducers";

const composeEnhancers =
    typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
        : compose;

const persistConfig = {
    key: "root",
    storage,
    whitelist: ["favorites"]
};

const persistedReducer = persistReducer(persistConfig, appReducer);

export default () => {
    const store = createStore(
        persistedReducer,
        composeEnhancers(applyMiddleware(apiMiddleware, thunk))
    );
    const persistor = persistStore(store);
    return { store, persistor };
};
