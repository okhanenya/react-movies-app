import React, { Component } from "react";
import { compose, isEmpty, isNil } from "ramda";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import { Switch, Route, withRouter } from "react-router-dom";

import Drawer from "@material-ui/core/Drawer";
import Favorite from "@material-ui/icons/Favorite";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { withStyles } from "@material-ui/core/styles";

import { fetchMovies } from "./actions";
import SearchResult from "./containers/SearchResult";
import Favorites from "./containers/Favorites";
import DetailMovie from "./containers/DetailMovie";
import Header from "./containers/Header";

const styles = theme => ({
    content: {
        padding: " 2.5% 2.5% 5% 2.5%"
    },
    toolbar: theme.mixins.toolbar
});

class App extends Component {
    state = {
        isDrawerOpen: false
    };

    static propTypes = {
        classes: PropTypes.object.isRequired,
        fetchMovies: PropTypes.func.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        history: PropTypes.object.isRequired
    };

    toggleDrawer = open => () =>
        this.setState({
            isDrawerOpen: open
        });

    render() {
        const { handleSubmit, classes, history, fetchMovies } = this.props;
        return (
            <div>
                <Header
                    onSearch={handleSubmit(({ movieName }) => {
                        if (!isEmpty(movieName) && !isNil(movieName)) {
                            fetchMovies(movieName).then(() => {
                                history.push({
                                    pathname: "/searchResult",
                                    state: { movieName }
                                });
                            });
                        }
                    })}
                    onMenuClick={open =>
                        this.setState({
                            isDrawerOpen: open
                        })
                    }
                />
                <div className={classes.content}>
                    <Switch>
                        <Route
                            exact
                            path="/searchResult"
                            component={SearchResult}
                        />
                        <Route exact path="/" component={Favorites} />
                        <Route exact path="/movie" component={DetailMovie} />
                    </Switch>
                </div>
                <Drawer
                    open={this.state.isDrawerOpen}
                    onClose={this.toggleDrawer(false)}
                >
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={this.toggleDrawer(false)}
                        onKeyDown={this.toggleDrawer(false)}
                    >
                        <List component="nav">
                            <div className={classes.toolbar} />
                            <ListItem button onClick={() => history.push("/")}>
                                <ListItemIcon>
                                    <Favorite />
                                </ListItemIcon>
                                <ListItemText primary="Favorites" />
                            </ListItem>
                        </List>
                    </div>
                </Drawer>
            </div>
        );
    }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {
    fetchMovies
};

export default compose(
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    reduxForm({
        form: "searchForm"
    }),
    withStyles(styles)
)(App);
