import React from "react";
import PropTypes from "prop-types";

import GridList from "@material-ui/core/GridList";
import { withStyles } from "@material-ui/core/styles";

import MoviesGridListItem from "./MoviesGridListItem";
import { isFavoriteMovie } from "../../utils";

const styles = () => ({
    root: {
        display: "flex",
        justifyContent: "center"
    }
});

const MoviesGridList = ({
    as,
    classes,
    favorites,
    movies,
    onMovieSelect,
    showFavoriteIcon
}) => (
    <GridList className={classes.root}>
        {movies.map((movie, index) => (
            <MoviesGridListItem
                movie={movie}
                onSelect={onMovieSelect}
                key={`${as}-MoviesGridListItem-${index}`}
                showFavoriteIcon={showFavoriteIcon}
                isFavorite={isFavoriteMovie(movie.id, favorites)}
            />
        ))}
    </GridList>
);

MoviesGridList.propTypes = {
    as: PropTypes.string,
    classes: PropTypes.object.isRequired,
    favorites: PropTypes.array.isRequired,
    movies: PropTypes.array.isRequired,
    onMovieSelect: PropTypes.func,
    showFavoriteIcon: PropTypes.bool
};

MoviesGridList.defaultProps = {
    as: "",
    favorites: [],
    onMovieSelect: () => {},
    showFavoriteIcon: false
};

export default withStyles(styles)(MoviesGridList);
