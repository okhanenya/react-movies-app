import React from "react";
import PropTypes from "prop-types";
import { Field } from "redux-form";

import { TextField } from "redux-form-material-ui";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Dehaze from "@material-ui/icons/Dehaze";
import Search from "@material-ui/icons/Search";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
    appBar: {
        zIndex: 2000,
        position: "relative"
    },
    menuContainer: {
        flex: 2
    },
    searchInput: {
        flex: 1,
        backgroundColor: "white",
        borderRadius: "5px",
        padding: "0px 4px 0px 4px",
        maxWidth: 200,
        minWidth: 120
    },
    menuNavItem: {
        display: "inline-block",
        padding: 4,
        cursor: "pointer"
    }
});

const Header = ({ classes, onSearch, onMenuClick }) => (
    <AppBar position="static" className={classes.appBar}>
        <Toolbar>
            <div className={classes.menuContainer}>
                <IconButton
                    className={classes.menuNavItem}
                    onClick={() => onMenuClick(true)}
                    aria-label="menu"
                    color="inherit"
                >
                    <Dehaze />
                </IconButton>
            </div>
            <form onSubmit={onSearch}>
                <Field
                    name="movieName"
                    component={TextField}
                    type="text"
                    className={classes.searchInput}
                    placeholder="search"
                />
                <IconButton
                    className={classes.menuNavItem}
                    aria-label="search"
                    color="inherit"
                    type="submit"
                >
                    <Search />
                </IconButton>
            </form>
        </Toolbar>
    </AppBar>
);

Header.propTypes = {
    classes: PropTypes.object.isRequired,
    onSearch: PropTypes.func,
    onMenuClick: PropTypes.func.isRequired,
};

Header.defaultProps = {
    onSearch: () => {}
};

export default withStyles(styles)(Header);
