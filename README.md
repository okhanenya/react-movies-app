# react-movies-app

###Nodes Code Test application

This is my Code Test application for Node, implementation using React.

###Start application
#### Install all project packges
`yarn`
#### Start server
`yarn start`

### Application pages

/ - favorites page, contain user library of movies.
/searchResult - result page, contains found movies by text value form search field.

