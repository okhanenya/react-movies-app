const initialState = {
    isFetching: false,
    isCompleted: false
};

export default types => {
    const [request, success, error] = types;

    return (state = initialState, action) => {
        switch (action.type) {
            case request:
                return { ...state, isFetching: !action.error };
            case success:
                return { ...state, isFetching: false, isCompleted: true };
            case error:
                return { ...state, isFetching: false };
            default:
                return state;
        }
    };
};
