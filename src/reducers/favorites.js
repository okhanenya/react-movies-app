import { findIndex, propEq, remove } from "ramda";
import ActionTypes from "../constants/ActionTypes";

const initialState = [];

const deleteMovieFromFavorites = (id, state) => {
    const index = findIndex(propEq("id", id))(state);
    if (index < 0) {
        return state;
    } else {
        return remove(index, 1)(state);
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.ADD_MOVIE_TO_FAVORITES:
            return [...state, action.payload];
        case ActionTypes.REMOVE_MOVIE_FROM_FAVORITES:
            return deleteMovieFromFavorites(action.payload.id, state);
        default:
            return state;
    }
};
