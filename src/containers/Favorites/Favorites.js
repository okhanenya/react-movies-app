import React from "react";
import PropTypes from "prop-types";
import { compose, isEmpty } from "ramda";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

import { getFavorites } from "../../reducers";
import MoviesGridList from "../../components/MoviesGridList";

const styles = () => ({
    root: { padding: "32px 24px" },
    title: { paddingBottom: 32 }
});

const Favorites = ({ classes, favorites, history }) => (
    <Paper className={classes.root}>
        <Typography variant="title" className={classes.title}>
            Favorites list
        </Typography>
        {!isEmpty(favorites) ? (
            <MoviesGridList
                as={"Favorites"}
                movies={favorites}
                onMovieSelect={id =>
                    history.push({
                        pathname: "/movie",
                        search: `?id=${id}`
                    })
                }
                key={"Favorites-MoviesGridList"}
            />
        ) : (
            <Typography>
                You dont have any favorites films. Please search and add some
                film in your list.
            </Typography>
        )}
    </Paper>
);

Favorites.propTypes = {
    classes: PropTypes.object.isRequired,
    favorites: PropTypes.array.isRequired,
    history: PropTypes.object.isRequired,
};

Favorites.defaultProps = {
    favorites: [],
};

const mapStateToProps = state => ({
    favorites: getFavorites(state)
});

const mapDispatchToProps = {};

export default compose(
    withRouter,
    withStyles(styles),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(Favorites);
