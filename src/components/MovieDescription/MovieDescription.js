import React from "react";
import PropTypes from "prop-types";

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core/styles";

const IMAGE_URL = "https://image.tmdb.org/t/p/original/";

const styles = () => ({
    root: {
        display: "flex",
        flexWrap: "wrap"
    },
    posterContainer: {
        flex: "0 1 251.21px"
    },
    movieTitle: {
        paddingBottom: 24
    },
    detailContainer: {
        flex: "1 0 260px",
        maxWidth: 1000,
        padding: 16,
        boxSizing: "border-box"
    }
});

const MovieDescription = ({
    addToFavorites,
    classes,
    isFavorite,
    movie,
    removeFromFavorites
}) => (
    <Paper>
        <div className={classes.root}>
            <div className={classes.posterContainer}>
                {movie.poster_path ? (
                    <img
                        src={`${IMAGE_URL}${movie.poster_path}`}
                        height={372.5}
                        width={251.21}
                        alt={movie.title}
                    />
                ) : (
                    <img alt={movie.title} />
                )}
            </div>
            <div className={classes.detailContainer}>
                <Typography variant="title" className={classes.movieTitle}>
                    {movie.title}
                </Typography>
                <Typography>
                    <b>Genres:</b>
                    {movie.genres &&
                        movie.genres.map(({ name }, i) => (
                            <span key={i}> {name} </span>
                        ))}
                </Typography>
                <Typography>
                    <b>Release date:</b> {movie.release_date}
                </Typography>
                <Typography>
                    <b>Runtime:</b> {movie.runtime} m.
                </Typography>
                <Typography>
                    <b>Status:</b> {movie.status}
                </Typography>
                <Typography>
                    <b>Vote average:</b> {movie.vote_average}
                </Typography>
                <br />
                <Typography>{movie.overview}</Typography>
                <br />
                <Button
                    variant="outlined"
                    color={isFavorite ? "secondary" : "primary"}
                    onClick={() =>
                        isFavorite
                            ? removeFromFavorites(movie)
                            : addToFavorites(movie)
                    }
                >
                    {isFavorite ? "Remove from favorites" : "Add to favorites"}
                </Button>
            </div>
        </div>
    </Paper>
);

MovieDescription.propTypes = {
    addToFavorites: PropTypes.func,
    classes: PropTypes.object.isRequired,
    isFavorite: PropTypes.bool,
    movie: PropTypes.object.isRequired,
    removeFromFavorites: PropTypes.func
};

MovieDescription.defaultProps = {
    addToFavorites: () => {},
    isFavorite: false,
    removeFromFavorites: () => {}
};

export default withStyles(styles)(MovieDescription);
