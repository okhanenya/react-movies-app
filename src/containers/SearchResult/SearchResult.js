import React from "react";
import PropTypes from "prop-types";
import { compose, isEmpty, path } from "ramda";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import LinearProgress from "@material-ui/core/LinearProgress";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

import MoviesGridList from "../../components/MoviesGridList";
import Pagination from "../../components/Pagination";
import { getMovies, getFavorites, isMoviesFetching } from "../../reducers";

const styles = () => ({
    root: {
        padding: "32px 24px"
    },
    topPaginationContainer: {
        display: "flex",
        justifyContent: "space-between",
        flexWrap: 'wrap',
        paddingBottom: 16
    },
    bottomPaginationContainer: {
        display: "flex",
        justifyContent: "flex-end",
        paddingTop: 16
    },
    resultTitle: {
        display: "flex",
        alignItems: "center"
    }
});

const SearchResult = ({
    classes,
    favorites,
    fetchMovies,
    history,
    isMoviesFetching,
    location,
    movies
}) => {
    const movieName = path(['state', 'movieName'])(location)
    return isMoviesFetching ? (
        <LinearProgress />
    ) : !isEmpty(movies.results) ? (
        <Paper className={classes.root}>
            <div className={classes.topPaginationContainer}>
                <Typography variant="title" className={classes.resultTitle}>
                    Results for "{movieName}" ({movies.total_results}).
                </Typography>
                <Pagination data={movies} fetch={fetchMovies} />
            </div>
            <MoviesGridList
                as={"SearchResult"}
                movies={movies.results}
                favorites={favorites}
                showFavoriteIcon
                onMovieSelect={id =>
                    history.push({
                        pathname: "/movie",
                        search: `?id=${id}`
                    })
                }
            />
            <div className={classes.bottomPaginationContainer}>
                <Pagination data={movies} fetch={fetchMovies} />
            </div>
        </Paper>
    ) : (
        <Paper className={classes.root}>
            <Typography variant="title">Result</Typography>
            <Typography>
                Something was wrong. Try find other movie, please :)
            </Typography>
        </Paper>
    );
};

SearchResult.propTypes = {
    classes: PropTypes.object.isRequired,
    favorites: PropTypes.array,
    fetchMovies: PropTypes.func,
    history: PropTypes.object.isRequired,
    isMoviesFetching: PropTypes.bool,
    location: PropTypes.object.isRequired,
    movies: PropTypes.object
};

SearchResult.defaultProps = {
    favorites: [],
    fetchMovies: () => {},
    isMoviesFetching: false,
    movies: {}
};

const mapStateToProps = state => ({
    movies: getMovies(state),
    favorites: getFavorites(state),
    isMoviesFetching: isMoviesFetching(state)
});

const mapDispatchToProps = {};

export default compose(
    withRouter,
    withStyles(styles),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(SearchResult);
