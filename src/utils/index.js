import { findIndex, propEq } from "ramda";

export const createKey = name => ({ [`${name}`]: name });
export const createKeysArray = array => array.map(key => createKey(key));
export const createKeys = array => Object.assign({}, ...createKeysArray(array));

export const isFavoriteMovie = (id, movies) => 
    findIndex(propEq("id", id))(movies) > -1;
