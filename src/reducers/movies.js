import ActionTypes from "../constants/ActionTypes";

const initialState = {
    page: 0,
    total_results: 0,
    total_pages: 0,
    results: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_MOVIES_SUCCESS:
            return action.payload;
        default:
            return state;
    }
};
