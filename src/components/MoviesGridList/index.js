import MoviesGridList from "./MoviesGridList";
export { default as MoviesGridList } from "./MoviesGridList";
export { default as MoviesGridListItem } from "./MoviesGridListItem";

export default MoviesGridList;
