import { createKeys } from "../utils";

export default createKeys([
    "FETCH_MOVIES_REQUEST",
    "FETCH_MOVIES_SUCCESS",
    "FETCH_MOVIES_FAILURE",

    "FETCH_MOVIE_REQUEST",
    "FETCH_MOVIE_SUCCESS",
    "FETCH_MOVIE_FAILURE",

    "ADD_MOVIE_TO_FAVORITES",
    "REMOVE_MOVIE_FROM_FAVORITES"
]);
