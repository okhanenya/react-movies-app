import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { parse } from "url";

import LinearProgress from "@material-ui/core/LinearProgress";

import MovieDescription from "../../components/MovieDescription";
import {
    addMovieToFavorits,
    fetchMovie,
    removeMovieFromFavorits
} from "../../actions";
import { getFavorites, getMovie, isMovieFetching } from "../../reducers";
import { isFavoriteMovie } from "../../utils";

class DetailMovie extends Component {
    static propTypes = {
        favorites: PropTypes.array.isRequired,
        isMovieFetching: PropTypes.bool,
        movie: PropTypes.object.isRequired,
        removeMovieFromFavorits: PropTypes.func,
        addMovieToFavorits: PropTypes.func
    };

    static defaultProps = {
        favorites: [],
        isMovieFetching: false,
        removeMovieFromFavorits: () => {},
        addMovieToFavorits: () => {}
    };

    componentDidMount() {
        const { id } = parse(document.URL, true).query;
        id && this.props.fetchMovie(id);
    }

    render() {
        const {
            favorites,
            isMovieFetching,
            movie,
            removeMovieFromFavorits,
            addMovieToFavorits
        } = this.props;
        return isMovieFetching ? (
            <LinearProgress />
        ) : (
            <MovieDescription
                movie={movie}
                addToFavorites={addMovieToFavorits}
                removeFromFavorites={removeMovieFromFavorits}
                isFavorite={isFavoriteMovie(movie.id, favorites)}
            />
        );
    }
}

const mapStateToProps = state => ({
    favorites: getFavorites(state),
    isMovieFetching: isMovieFetching(state),
    movie: getMovie(state)
});

const mapDispatchToProps = {
    addMovieToFavorits,
    fetchMovie,
    removeMovieFromFavorits
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DetailMovie);
