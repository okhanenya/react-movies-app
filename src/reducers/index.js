import { path } from "ramda";
import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import ActionTypes from "../constants/ActionTypes";
import movie from "./movie";
import movies from "./movies";
import favorites from "./favorites";
import fetching from "./fetching";

export default combineReducers({
    movie,
    movies,
    favorites,
    form: formReducer,
    isMoviesFetching: fetching([
        ActionTypes.FETCH_MOVIES_REQUEST,
        ActionTypes.FETCH_MOVIES_SUCCESS,
        ActionTypes.FETCH_MOVIES_FAILURE
    ]),
    isMovieFetching: fetching([
        ActionTypes.FETCH_MOVIE_REQUEST,
        ActionTypes.FETCH_MOVIE_SUCCESS,
        ActionTypes.FETCH_MOVIE_FAILURE
    ])
});

export const getMovies = path(["movies"]);
export const getMovie = path(["movie"]);
export const getFavorites = path(["favorites"]);

export const isMoviesFetching = path(["isMoviesFetching", "isFetching"]);
export const isMovieFetching = path(["isMovieFetching", "isFetching"]);
